import yargs from "yargs";
import { hideBin } from "yargs/helpers";
import * as pkg from "fs-extra";
import { normalize } from "path";
import {
  unlinkSync,
  rmdirSync,
  lstatSync,
  existsSync,
  readdirSync,
  symlinkSync,
  mkdirSync,
  copyFileSync
} from "fs";
import chalk from "chalk";

const { readFileSync } = pkg;

export function parseOptions() {
  return yargs(hideBin(process.argv)).argv;
}

export function getDirectories(source) {
  return readdirSync(normalize(source), { withFileTypes: true })
    .filter((dirEnt) => dirEnt.isDirectory())
    .map((dirEnt) => dirEnt.name);
}

export function getNdzConfig(projectPath) {
  const rawdata = readFileSync(normalize(`${projectPath}/project.ndz`));
  return JSON.parse(rawdata);
}

export function deleteFolderRecursive(path) {
  if (existsSync(path)) {
    readdirSync(path).forEach(function (file) {
      const curPath = path + "/" + file;
      if (lstatSync(curPath).isDirectory()) {
        // recurse
        deleteFolderRecursive(curPath);
      } else {
        // delete file
        unlinkSync(curPath);
      }
    });
    rmdirSync(path);
  }
}

export function createSymlink(source, target, type = "dir") {
  if (!existsSync(target)) {
    symlinkSync(source, target, type);
    return true;
  } else {
    return false;
  }
}

export function ensureDirectoryExistence(filePath) {
  if (!existsSync(filePath)) {
    mkdirSync(filePath, { recursive: true });
  }
}

export function getModsKeys(mods) {
  return mods
    .flatMap((modPath) => {
      const keysFolder = normalize(`${modPath}`);
      if (existsSync(keysFolder)) {
        const keys = readdirSync(keysFolder, { recursive: true })
          .filter((file) => file.endsWith(".bikey"))
          .map((f) => {
            return {
              path: normalize(`${keysFolder}/${f}`),
              filename: f.replace(/^.*[\\\/]/, ""),
            };
          });
        return keys;
      } else {
        console.warn(
          chalk.bgYellow(`Folder doesn't exists, this mod won't be loaded:`),
          keysFolder
        );
      }
    })
    .filter((f) => f != undefined);
}

export function getModsPbo(mods) {
  const lm = mods.flatMap((m) => {
    const pbos = readdirSync(normalize(`${m}`), { recursive: true })
      .filter((e) => e.endsWith(".pbo"))
      .map((e) => {
        return {
          path: normalize(`${m}/${e}`),
          filename: e.replace(/^.*[\\\/]/, ""),
        };
      });

    return pbos;
  })
  .filter((f) => f != undefined);

  return lm;
}

export function copyKeys(keyPath, destintaionPath) {
  try {
    if (!existsSync(keyPath)) {
      console.error(`${keyPath} doesn't exists`);
      return;
    }
  } catch (err) {
    console.error(err);
    return;
  }

  // console.log(chalk.green("Adding keys to build..."));
  copyFileSync(keyPath, destintaionPath);
  // console.log(chalk.green(`${keyPath} was copied to ${destintaionPath}`));
}

export class Log {
  static error(message, ...args) {
    console.error(chalk.bgRed(message), ...args);
  }

  static warn(message, ...args) {
    console.warn(chalk.bgYellow(message), ...args);
  }

  static info(message, ...args) {
    console.info(chalk.bgGrey(message), ...args);
  }

  static debug(message, ...args) {
    if (parseOptions().debug) {
      console.info(`DEBUG: ${chalk.bgGrey(message)}`, ...args);
    }
  }

  static message(message, ...args) {
    console.log(chalk.bgGreen(message), ...args);
  }

  static process(message, ...args) {
    console.log(chalk.cyan(message), ...args);
  }
}
