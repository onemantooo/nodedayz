export const config  = {
    addonBuilderPath: "E:/SteamLibrary/steamapps/common/DayZ Tools/Bin/AddonBuilder/AddonBuilder.exe", // Path to AddonBuilder
    keyPath: "P:/keys/", // Path to folder with your keys (bikey, biprivatekey)
    keyName: "onemantooo",  // keyname, for example if your key is onemantooo.biprivatekey - you should write 'onemantooo'
    copyKeyInBuild: true, // If true - bikey will be copied into builded mod, in Keys folder
    includeFile: "include.txt", // AddonBuilder include params
    excludeFile: "exclude.txt", // AddonBuilder exclude params (ignored by now)
    dayz: {
        dayzPath: "E:/SteamLibrary/steamapps/common/DayZ/", // Path to DayZ folder
        dayzFile: "DayZDiag_x64.exe", // DayZ executable name (use diag to avoid errors with BE)
        playerName: "onemantooo", // DayZ player name
        additionalArgs: [ // Additional args which will be passed to DayZ executable
            "-skipIntro",
            "-dologs",
            "-adminlog",
            "-filePatching",
            "-freezecheck",
            "-window",
            "-newErrorsAreWarnings=1"
        ]
    },
    dayzServer: {
        dayzServerPath: "E:/SteamLibrary/steamapps/common/DayZServer/", // Path to DayZServer folder
        dayzServerFile: "DayZServerDiag_x64.exe", // DayZServer executable name (copy DayZDiag_x64 from DayZ folder)
        keysFolder: "keys", // Folder with server keys
        configFile: "serverNDZ.cfg", // local configuration file, it's in root of this project, you can modify it in any time
        port: "2302", // server port
        cpuCount: 4, // cpu count
        profilesPath: "profiles", // path to profiles folder (in DayZServer folder)
        additionalArgs: [ // Additional args which will be passed to DayZServer executable
            "-dologs",
            "-adminlog",
            "-netlog",
            "-freezecheck",
            "-filePatching",
            "-newErrorsAreWarnings=1"
        ]
    },
    modList: [ // List of mods from workshop, which wiil be loaded with your mods on server/client startup (npm run start)
        "@Dabs Framework",
        "@DayZ Editor Loader",
        "@CF"
    ]
}