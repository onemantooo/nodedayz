# NodeDZ

## Getting started

To make it easy for you to get started with NodeDZ, here's a list of recommended next steps.

Required tools:

- [ ] [Node.js](https://nodejs.org/en) (LTE Recommended)
- [ ] DayZ
- [ ] DayZ Server (if you wish to test your code, if you brave enough you can ignore it)
- [ ] AddonBuilder (DayZ Tools)
- [ ] Workbench if you need realtime debugging (DayZ Tools)
- [ ] DS Utils (once, to create your private key) (DayZ Tools)

Once you installed all the tools, you need to properly configure project and tools.

## Clone this repository

- [ ] Clone this Git repository with the following command:

```
git clone https://gitlab.com/onemantooo/nodedayz.git
```
> Or download as zip

* [ ] Delete hidden `.git` folder (optional)
* [ ] Initialize your git repository if you need to (optional)
* [ ] Open terminal within ndz folder and run command `npm i`

#### Dayz

No additional configuration needed

#### DayZServer

* [ ] Go to folder where you installed DayZ and find a file named `DayZDiag_x64.exe`
* [ ] Copy `DayZDiag_x64.exe` into a folder where DayZServer is installed and rename it into `DayZServerDiag_x64.exe`

> You will need to repeat that after every DayZ update.

#### AddonBuilder

No additional configuration needed

#### Workbench

No additional configuration wiil be presented here (at least for now), google it if you need.

#### DS Utils

Open DayZ Tools, click DS Utils and follow [this guide](https://www.youtube.com/watch?v=GX_Y0tICYQA&t=0) to create your keys, then put it into some directory

#### NDZ configuration

Well, now it's time to configure NDZ tool.

* [ ] Open ndzconf.mjs in project folder
* [ ] Replace values of variables
  * [ ] `addonBuilderPath` - must be path to your installed AddonBuilder
  * [ ] `keyPath` - must be path to folder in which you stored keys from "DS Utils" step
  * [ ] `keyName` - must be name of your keys
  * [ ] `dayz.dayzPath` - path to folder where DayZ is installed
  * [ ] `dayzServer.dayzServerPath` - path to folder where DayZServer is installed
  * [ ] `modList` - list of additional mods to load (mods must exists in your {DayZFolder}/!Workshop i.e. you must subscribe, and load them through DayZ client before adding it here, and also put keys for this mods into {DayZServerFolder}/keys)

## Initialize a new mod

You can easily create a boilerplate for your mod with command:

`npm run gen -- --name=PF_Test --client --server`

Where:

- [ ] `--name` - name of your mod
- [ ] `--client` - is used if you creating clientside mod (you still can write serveside code in it)
- [ ] `--server` - is used if you want to separate some code to server only, or just need only server code
- [ ] At least one (`--client` or `--server`) required

This command will create default files and folders. NDZ using folder structure to determine what to build, so don't change it, just create your files/folders inside generated. Also it will generate default `config.cpp`, feel free to change it if you need, but don't touch `defs`.

> NDZ supports multiple mods handling, so if you want add new mod to your project, just run a command, name ofcourse should be different.

## Building your mod

To build all your mods(projects) just run command:

`npm run build`

You can also build only one project if you need to, by calling `npm run build --project={Your Project Name}`

`npm run build --project=PF_Test`

All builded files are dropped into `./build` folder, so you can easily distribute them from here. But you can also run it from there with one command.

## Running your mods

To run all builded mods(from `build` folder), you can use command:

`npm run start`

Workshop mods from ndzconf.mjs:modList - will be also included, also NDZ will automatically copy keys for server - if they present within mod, and also will remove keys on stop.

Also you can build and run you mods with single command:

`npm run debug`

And start only server/client if needed for some reason, like:

`npm run start:client` or `npm run start:server`

## Mounting your mod to use in Workbench without collisions

Your mod can be placed anywhere in the system, but sometimes you need to view some things in Workbench.

To do so you can run `npm run link -- --project={Your Project Name}`, this will mount your mod to P drive, so you can access it from Workbench.

Note: Your project name is either name of Client side or Server side. i.e. `npm run link -- --project=EdgeCore` if it's client, or `npm run link -- --project=EdgeCoreServer` if it's server.

To delete link - just delete folder link from P drive.

## License

MIT - Fell free to use it in any ways
