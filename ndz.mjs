import { config } from "./ndzconf.mjs";
import { execFile } from "child_process";
import { normalize } from "path";
import {
  parseOptions,
  ensureDirectoryExistence,
  createSymlink,
  getDirectories,
  getModsKeys,
  copyKeys,
  getModsPbo,
  Log,
} from "./utils.mjs";
import * as pkg from "fs-extra";
import {
  existsSync,
  copyFileSync,
  unlinkSync,
  rmSync,
  readdirSync,
  readFileSync,
  writeFileSync,
} from "fs";
import { execFileSync } from "child_process";
import nunjucks from "nunjucks"


const { emptyDirSync } = pkg;
const options = parseOptions();
const PROJECTS_DIRECTORY = normalize(`${process.cwd()}/projects`);
const BUILD_DIRECTORY = normalize(`${process.cwd()}/build`);
const TEMP_DIRECTORY = normalize(`${process.cwd()}/temp`);
const TEMPLATES_DIRECTORY = normalize(`${process.cwd()}/templates`);
const SERVERMOD_DIRECTORY = normalize(`${process.cwd()}/servermods`);
const SERVERMOD_DIRECTORY_ADDONS = normalize(`${SERVERMOD_DIRECTORY}/Addons`);
const SERVER_CONFIG_FILE = normalize(
  `${process.cwd()}/${config.dayzServer.configFile}`
);

const executions = [];
let unlinkTargets = [];
let clearingRunning = false;

function clearEverything() {
  if (!clearingRunning) {
    clearingRunning = true;
  } else {
    return;
  }

  executions.forEach((e) => {
    if (!e.process.killed) {
      Log.info(`Stopping process path: ${e.path} pid: ${e.process.pid}`);
      e.process.kill("SIGKILL");
    }
  });

  if (unlinkTargets.length) {
    Log.info(`Unlinking ${unlinkTargets.length} files`)
  }
  unlinkTargets.forEach((e) => {
    Log.debug(`Unlinking: ${e.link}`);
    rmSync(e.link);
  });

  process.exitCode = 0;
}

process.on("SIGINT", clearEverything);

function build() {
  if (options.project) {
    // Build specific project
    Log.info(`Building single project: ${options.project}`);
    buildProject(`${options.project}`);
  } else {
    // Build all existing projects
    const projectsList = getDirectories(PROJECTS_DIRECTORY);
    Log.info(`Building projects:`, projectsList);
    projectsList.forEach((projectName) => {
      buildProject(projectName);
    });
  }

  return Promise.resolve();
}

function serve(type) {
  const projectModsList = getDirectories(BUILD_DIRECTORY).map((lm) =>
    normalize(`${BUILD_DIRECTORY}/${lm}`)
  );
  const projectServerModsList = projectModsList.filter((m) =>
    m.endsWith("Server")
  );
  const projectClientModsList = projectModsList.filter(
    (m) => !projectServerModsList.includes(m)
  );

  const externalClientMods = getExternalModList();
  const externalServerMods = getDirectories(SERVERMOD_DIRECTORY).map(
    (lm) => `${SERVERMOD_DIRECTORY}/${lm}`
  );

  const resultModsList = externalClientMods.concat(projectClientModsList);

  if (type == "server") {
    const symlinkedKeysList = createKeySymlinks(getModsKeys(resultModsList));
    const symlinkedServerMods = createServerModSymlinks(projectServerModsList);

    symlinkedKeysList.forEach((k) => unlinkTargets.push(k));
    symlinkedServerMods.forEach((s) => unlinkTargets.push(s));

    startServer(resultModsList);
  } else if (type == "client") {
    startClient(resultModsList);
  } else {
    throw new Error("Unsupported serve type");
  }
}

function startServer(mods) {
  let args = [
    `-server`,
    `-config=${SERVER_CONFIG_FILE}`,
    `-port=${config.dayzServer.port}`,
    `-cpuCount=${config.dayzServer.cpuCount}`,
    `-profiles=${config.dayzServer.profilesPath}`,
    `-serverMod=${SERVERMOD_DIRECTORY}`,
  ];

  if (
    config.dayzServer.additionalArgs &&
    config.dayzServer.additionalArgs.length
  ) {
    config.dayzServer.additionalArgs.forEach((a) => args.push(a));
  }

  args.push(`-mod=${mods.join(";")}`);

  Log.info(`Starting server...`);

  Log.debug(args);

  const filePath = normalize(
    `${config.dayzServer.dayzServerPath}${config.dayzServer.dayzServerFile}`
  );
  const serverProcess = execFile(
    filePath,
    args,
    function (err, data) {
      if (err) {
        Log.debug(err);
      }
      clearEverything();
      Log.debug(data.toString());
    }
  );

  executions.push({
    path: filePath,
    process: serverProcess,
  });
}

function startClient(mods) {
  let args = [`"onemantooo"`, `-connect=127.0.0.1:${config.dayzServer.port}`];

  if (config.dayz.additionalArgs && config.dayz.additionalArgs.length) {
    config.dayz.additionalArgs.forEach((e) => args.push(e));
  }

  args.push(`-mod=${mods.join(";")}`);

  Log.info(`Starting client...`);
  Log.debug(args);

  const filePath = normalize(`${config.dayz.dayzPath}${config.dayz.dayzFile}`);

  const clientProcess = execFile(
    filePath,
    args,
    function (err, data) {
      if (err) {
        Log.debug(err);
      }
      clearEverything();
      Log.debug(data.toString());
    }
  );

  executions.push({
    path: filePath,
    process: clientProcess,
  });
}

function createKeySymlinks(keys) {
  Log.info("Creating key symlinks for selected and builded mods...");
  let alreadyExistsKeys = 0;
  const successfullKeySymlinks = keys
    .map((key) => {
      const destination = normalize(
        `${config.dayzServer.dayzServerPath}${config.dayzServer.keysFolder}/${key.filename}`
      );
      if (createSymlink(key.path, destination, "file")) {
        key.link = destination;
        return key;
      }
      const source = readFileSync(key.path);
      const target = readFileSync(destination);
      if (source.equals(target)) {
        if (options.verbose) {
          Log.warn("Key file already exists, skipping:", destination);
        } else {
          alreadyExistsKeys += 1;
        }
      } else {
        Log.error(
          `Key file already exists and has different content (skipped, but my cause some mod not to load):`,
          `${key.path} -> ${destination}`
        );
      }
      return;
    })
    .filter((e) => e != null);

  if (alreadyExistsKeys) {
    Log.warn(
      `${alreadyExistsKeys} keys already exists and was skipped. Use --verbose to find out which keys.`
    );
  }
  Log.info(`Created ${successfullKeySymlinks.length} key symlinks`);

  return successfullKeySymlinks;
}

function createServerModSymlinks(mods) {
  const pboPaths = getModsPbo(mods);

  const symlinkedPbos = pboPaths
    .map((p) => {
      const destination = normalize(
        `${SERVERMOD_DIRECTORY_ADDONS}/${p.filename}`
      );
      if (createSymlink(p.path, destination, "file")) {
        p.link = destination;
        return p;
      }

      Log.error(
        "Mod file already exists - probably you need to delete it manually. Skipping:",
        destination
      );
      return;
    })
    .filter((e) => e != null);

  Log.info(`Created ${symlinkedPbos.length} server mod symlinks`);

  return symlinkedPbos;
}

function getProjectPath(projectName) {
  const projectPath = normalize(`${PROJECTS_DIRECTORY}/${projectName}`);
  if (!existsSync(projectPath)) {
    throw new Error(`${projectPath} doesn't exists!`);
  }
  return projectPath;
}

function buildProject(projectName) {
  const projectPath = getProjectPath(projectName);
  if (!existsSync(projectPath)) {
    throw new Error(`${projectPath} doesn't exists!`);
  }
  const serverSidePath = normalize(`${projectPath}/src/Server`);
  const clientSidePath = normalize(`${projectPath}/src/Client`);
  const IsClientSideExists = existsSync(clientSidePath);
  const IsServerSideExists = existsSync(serverSidePath);

  if (!IsClientSideExists && !IsServerSideExists) {
    Log.error(
      `${projectName}:`,
      "No client or server side found, no files to build. Skipping."
    );
    return;
  }

  Log.info(`${projectName}: Packing started:`);

  if (IsClientSideExists) {
    Log.info(`${projectName}: Found client side, packing...`);
    packPbo(projectName, clientSidePath);
  }

  if (IsServerSideExists) {
    Log.info(`${projectName}: Found server side, packing...`);
    packPbo(`${projectName}Server`, serverSidePath, false);
  }

  Log.info(`${projectName}: Packing done!`);
}

function packPbo(name, path, copyKeysInBuild = true) {
  const destinationPath = normalize(`${BUILD_DIRECTORY}/${name}`);
  const symlinkPath = normalize(`${TEMP_DIRECTORY}/${name}`);
  const outputModPath = normalize(`${BUILD_DIRECTORY}/${name}/Addons`);
  const includeFile = normalize(`${process.cwd()}/${config.includeFile}`);
  // const excludeFile = normalize(`${__dirname}/${config.excludeFile}`);
  emptyDirSync(destinationPath);
  ensureDirectoryExistence(TEMP_DIRECTORY);
  createSymlink(path, symlinkPath);

  const ABArgs = [
    symlinkPath,
    outputModPath,
    `-include=${includeFile}`,
    `-packonly`,
    // `-exclude=${excludeFile}`, //rewrite to pboproject
    `-clear=true`,
    `-sign=${config.keyPath}${config.keyName}.biprivatekey`,
  ];

  execFileSync(config.addonBuilderPath, ABArgs);

  if (
    copyKeysInBuild &&
    (!config.keyPath || !config.keyName || !config.copyKeyInBuild)
  ) {
    Log.warn(
      "Keys to copy not present, skipping. Fill `config.keyPath` && `config.keyName` && `config.copyKeyInBuild` to copy keys in build."
    );
  } else if (copyKeysInBuild) {
    const KeysDirectory = `${destinationPath}/Keys`;
    const keyForBuild = normalize(`${config.keyPath}${config.keyName}.bikey`);
    const copyKeyTarget = normalize(`${KeysDirectory}/${config.keyName}.bikey`);

    ensureDirectoryExistence(KeysDirectory);
    Log.info(`${name}: Adding keys to build...`);
    copyKeys(keyForBuild, copyKeyTarget);
  }
}

const getExternalModList = function () {
  const modListWithPath = config.modList
    .map((el) => {
      return normalize(`${config.dayz.dayzPath}!Workshop/${el}`);
    })
    .filter((modPath) => {
      if (!existsSync(modPath)) {
        Log.warn(
          `Folder doesn't exists, this mod is excluded and won't be loaded:`,
          modPath
        );
        return false;
      }
      return true;
    });

  return modListWithPath;
};

function generate(projectName){
  const projectDirectory = normalize(`${PROJECTS_DIRECTORY}/${projectName}`);
  const projectDirectoryClient = normalize(`${projectDirectory}/src/Client`);
  const projectDirectoryServer = normalize(`${projectDirectory}/src/Server`);
  if (existsSync(projectDirectory)) {
    Log.warn(`Project with name '${projectName}' already exists!`);
  }

  if (!options.server && !options.client) {
    Log.error(`No options '--server' or '--client' provided, specify at least one`);
    return;
  }
  
  ensureDirectoryExistence(projectDirectory);

  if (options.client) {
    if (existsSync(projectDirectoryClient)) {
      Log.error(`Client side for '${projectName}' already exists! Skipping.`);
    } else {
      ensureDirectoryExistence(projectDirectoryClient);
      generateDefaults(projectDirectoryClient, projectName);
      Log.info(`Generated client side for ${projectName}`);
    }
  }
  if (options.server) {
    if (existsSync(projectDirectoryServer)) {
      Log.error(`Server side for '${projectName}' already exists! Skipping.`);
    } else {
      ensureDirectoryExistence(projectDirectoryServer);
      generateDefaults(projectDirectoryServer, projectName, true);
      Log.info(`Generated server side for ${projectName}`);
    }
  }
}

function generateDefaults(path, projectName, isServer=false) {
  const projectRealName = projectName + (isServer ? 'Server' : '');
  const folderNames = ["0_Preload", "1_Core", "2_GameLib", "3_Game", "4_World", "5_Mission"];
  if (!isServer) {
    folderNames.push("Data");
    folderNames.push("Layouts");
    folderNames.push("Sounds");
  }
  folderNames.forEach((f) => {
    ensureDirectoryExistence(normalize(`${path}/${f}`));
  });

  const preloadPathCmn = normalize(`${path}/0_Preload/Common`);
  ensureDirectoryExistence(preloadPathCmn);
  writeFileSync(normalize(`${preloadPathCmn}/${projectName}_Defines.c`), `#define ${projectRealName.toUpperCase()}`);

  const configFilePath = normalize(`${path}/config.cpp`);
  writeFileSync(configFilePath, nunjucks.render(normalize(`${TEMPLATES_DIRECTORY}/config.cpp.template`), {
    ProjectName: projectRealName
  }));
}

function linkProject(projectName) {
  const projectPath = getProjectPath(projectName);
  const projectPathClient = normalize(`${projectPath}/src/Client`);
  createSymlink(projectPathClient, normalize(`P:/${projectName}`));
}


if (options.build) {
  build();
  rmSync(TEMP_DIRECTORY, { recursive: true, force: true });
} else if (options.serve) {
  if (options.server) {
    serve("server");
  }
  if (options.client) {
    serve("client");
  }
} else if (options.gen) {
  if (!options.name) {
    Log.error("No name provided for your mod, use --name={name}")
  } else {
    generate(options.name);
  }
} else if (options.link) {
  linkProject(options.project);
}

